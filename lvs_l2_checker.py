#!/usr/bin/env python3
# SPDX-License-Identifier: Apache-2.0
# lvs_l2_checker.py: Checks that all nodes in all load balancer pools are connected
# at layer 2. Logs an error if not.
# Author: Brian King

import configparser
import dns.resolver
import json
import logging
import requests
import os
import subprocess
import sys

__title__ = __doc__
logger = logging.getLogger(__name__)

LVS_CFG_PATH = '/etc/pybal/pybal.conf'
POOLS_URL = 'https://config-master.wikimedia.org'


def read_lvs_cfg():
    """ Create configparser object so we can read pybal config """
    logger.info('Reading pybal config from {}'.format(LVS_CFG_PATH))
    if os.path.isfile(LVS_CFG_PATH):
        lvs_cfg = configparser.ConfigParser()
        lvs_cfg.read(LVS_CFG_PATH)
        return lvs_cfg


def get_datacenter(lvs_cfg):
    """ Reads this specific LVS host's datacenter from pybal config """
    for section in lvs_cfg.sections():
        if section != 'global':
            for k, v in lvs_cfg.items(section):
                if k == 'config':
                    datacenter = v.split('pools/')[1].split('/')[0]
                    logger.info('Found datacenter: {}'.format(datacenter))
                    return datacenter


def get_pool_names(lvs_cfg):
    """ Reads this specific LVS host's pool names from pybal config """
    pool_names = []
    logger.info('Collecting all LVS pools on this host...')
    for section in lvs_cfg.sections():
        # 'global' section doesn't contain any pool info
        if section != 'global':
            lb_pool = section.split('_')[0]
            pool_names.append(lb_pool)
    logger.info("Found {} lb pools in pybal".format(len(pool_names)))
    return pool_names

# temporarily disabled until we figure out why some service's pools
# don't appear in pools.json ; see T364037


def get_all_pool_info():
    """ Reads all lb pool info from config-master"""
    pools_path = "{}/pools.json".format(POOLS_URL)
    pools_response = requests.get(url=pools_path)
    pools = pools_response.json()
    return pools


def get_backend_nodes(datacenter, pool_names, pools):
    """ Reads all backend node data and creates a list of their hostnames """
    backend_nodes = []
    for pool in pool_names:
        try:
            for node in pools[datacenter][pool].keys():
                backend_nodes.append(node)
        # Multiple frontend services can share a single backend pool.
        # LVS contains only the backend info, while pools.json (our source
        # for the backends) contains frontends as well. Ignore the missing
        # frontend names, as we only care about backends (ref T364037).
        except KeyError:
            continue
    logger.info('Found {} backend nodes'.format(set(backend_nodes)))
    return (list(set(backend_nodes)))


def resolve_aaaa_records(node):
    """ Resolve AAAA records of node """
    resolver = dns.resolver.Resolver()
    try:
        node_aaaa_query = resolver.resolve(node, 'AAAA')
    # not all hosts have an IPv6 address
    except dns.resolver.NoAnswer:
        logger.info("Node {} has no AAAA record".format(node))
        return
    for rdata in node_aaaa_query:
        return rdata.address


def resolve_a_records(node):
    """ Resolve A records of node """
    resolver = dns.resolver.Resolver()
    try:
        node_a_query = resolver.resolve(node, 'a')
    except dns.resolver.NoAnswer:
        # we do expect an A record. Not critical though, as anything w/out an A
        # record will presumably fail health checks.
        logger.warning("WARNING: Node {} has no a record".format(node))
        sys.exit(1)
    for rdata in node_a_query:
        return rdata.address


def resolve_node_ips(backend_nodes):
    """Look up A/AAAA records for all pool nodes and add them to a list of
    dicts"""
    node_dns_records = []
    logger.info('Looking up DNS records for all pool nodes...')
    for node in backend_nodes:
        backend_node = {}
        backend_node['hostname'] = node
        backend_node['ipv6'] = resolve_aaaa_records(node)
        backend_node['ipv4'] = resolve_a_records(node)
        node_dns_records.append(backend_node)
    return node_dns_records


def get_route(ip_addr):
    """get routes via ip route shell cmd """
    ip_cmd = "/usr/bin/ip --json route get fibmatch {}".format(ip_addr)
    # example output for L2 connectivity (good): [{'dst': '10.192.32.0/22', 'dev': 'eno1',
    # 'protocol': 'kernel', 'scope': 'link', 'prefsrc': '10.192.32.49', 'flags': []}]

    # example for L3 connectivity only (bad): [{'dst': 'default', 'gateway': '10.192.32.1',
    # 'dev': 'eno1', 'flags': ['onlink']}]
    ip_route_cmd = subprocess.run(ip_cmd, shell=True,
                                  capture_output=True)
    ip_route_return = json.loads(ip_route_cmd.stdout)
    ip_route = ip_route_return[0].keys()
    return ip_route


def verify_l2_connection(hostname, route_data):
    """check L2 connection via ip route data """
    if 'gateway' in route_data:
        logger.error('CRITICAL: Node {} not connected at \
layer 2'.format(hostname))
        sys.exit(2)


def check_l2_connection(node_dns_records):
    """perform L2 checks for all backend nodes via A/AAAA records """
    logger.info('Checking L2 connectivity for all pools associated with this\
host...')
    for record in node_dns_records:
        hostname = record['hostname']
        ip_route = get_route(record['ipv4'])
        verify_l2_connection(hostname, ip_route)
        if record['ipv6']:
            ip6_route = get_route(record['ipv6'])
            verify_l2_connection(hostname, ip6_route)
    # If we make it this far, we (naively?) believe everything is OK.
    print('OK: All nodes reachable at layer 2')
    sys.exit(0)


def main():
    lvs_cfg = read_lvs_cfg()
    datacenter = get_datacenter(lvs_cfg)
    pool_names = get_pool_names(lvs_cfg)
    pools = get_all_pool_info()
    backend_nodes = get_backend_nodes(datacenter, pool_names, pools)
    node_dns_records = resolve_node_ips(backend_nodes)
    check_l2_connections(node_dns_records)


if __name__ == '__main__':
    main()
