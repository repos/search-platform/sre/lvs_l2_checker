=== LVS L2 Checker ===

Reads pybal config from an LVS host and confirms that all nodes known by the active pybal config are in the same layer 2 domain as the LVS host.
